# nibl_file_renamer
This is a small program that replaces every underscore through a space because the bots of https://nibl.co.uk/bots.php sometimes offer a file with underscores instead of spaces.