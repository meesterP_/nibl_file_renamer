use std::fs;

fn main() {
    println!("Renaming files.");
    let paths = fs::read_dir("./").expect("Could not open directory."); // just panic if we couldn't open the root directory
    for file in paths { // iterate through every file
        let dir_entry = match file {
            Ok(t) => t,
            Err(e) => {
                eprintln!("{}", e);
                continue; // if we got an error while opening the file just go to the next file
            }
        };
        let old_string = dir_entry.path().to_string_lossy().to_string(); // get the filename as string
        let new_string = old_string.replace("_", " "); // replace every _ with a space
        match fs::rename(old_string, new_string) { // give the file the new name
            Ok(_) => println!("Renamed file."),
            Err(e) => eprintln!("{}", e)
        }
    }
    println!("Done.");
}